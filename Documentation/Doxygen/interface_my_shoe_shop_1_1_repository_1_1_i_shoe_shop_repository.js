var interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository =
[
    [ "Create", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#a577f1656a6ee3f33f46c7e34c400fa0e", null ],
    [ "Create", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#acd6cd14999ce1a9df6ce916ea586a095", null ],
    [ "Create", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#a1b98f9a7e15650b0459e599efefc0a74", null ],
    [ "DeleteBeszallito", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#af4e9e0c37d5dac35be946f81e03eecd8", null ],
    [ "DeleteCipo", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#a64a14f44053e1f82ad616d5cf533e1af", null ],
    [ "DeleteVasarlo", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#a176300215503c49452a469356a8400c4", null ],
    [ "ReadAllBeszallito", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#a078fb0b2a19dd6f3cf3d5941765a04ce", null ],
    [ "ReadAllCipo", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#afb1792a91fc034f191d6f9657f7d8af6", null ],
    [ "ReadAllVasarlo", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#a5881c0eea619f748b09ae9ac23a474f4", null ],
    [ "UpdateBeszallitoNepszeruseg", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#a359e96ae31cbad809d87d2e341efa43d", null ],
    [ "UpdateCipoAr", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#ae2583897dfcbd24519ecf19f3cca60c2", null ],
    [ "UpdateCipoNev", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#ad95cd7e33823761724c999f75152d8fe", null ],
    [ "UpdateVasarloCipo", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html#a8f65b422b6515f5ccec8beee6c982e9d", null ]
];