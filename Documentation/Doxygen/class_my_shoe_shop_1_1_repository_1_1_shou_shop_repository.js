var class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository =
[
    [ "ShouShopRepository", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#af42d550314530a197e8b3ae4632d6668", null ],
    [ "Create", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#a52ce959e60e60409cb53199f93d3678e", null ],
    [ "Create", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#a91fa2e2ac6dcaf6d1731149585b16cc6", null ],
    [ "Create", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#a27b9c2e6a9bf2565f6b9b41f3991939f", null ],
    [ "DeleteBeszallito", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#aae3ec49ade92dfafb9a9df51564a3fba", null ],
    [ "DeleteCipo", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#a87dac6f365554a18d9236ca11c4d867a", null ],
    [ "DeleteVasarlo", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#abb3d130e08d72008d095e26cd1394c5c", null ],
    [ "ReadAllBeszallito", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#a54a56143c969897f095b9bbfbd0052de", null ],
    [ "ReadAllCipo", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#a0f03abe886a8021946de2b32c4fa1d40", null ],
    [ "ReadAllVasarlo", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#ae16712051b225b8021275c6172f4d803", null ],
    [ "UpdateBeszallitoNepszeruseg", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#a3a4d6dfcbc6672c5b5527f615b31055b", null ],
    [ "UpdateCipoAr", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#a1c1021b806b662c44aceff82422a5b06", null ],
    [ "UpdateCipoNev", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#a938aa03821fff3dd586bbd2f7bbeacbf", null ],
    [ "UpdateVasarloCipo", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html#ab26b3da3b26cf2012876d17670122473", null ]
];