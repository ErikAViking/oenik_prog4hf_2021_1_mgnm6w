var interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic =
[
    [ "CipokAtlagAr", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html#a2d1ff4602f22d79b24089a3e39eadca5", null ],
    [ "CipokBeszallitonkent", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html#ad7244c382683d5e56d6b716673dd8007", null ],
    [ "CreateBeszallito", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html#acd606827eb2fb8dee3c1ed85449bf8be", null ],
    [ "CreateCipo", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html#a88fe2486f0393d250be36beeff7ee7a2", null ],
    [ "CreateVasarlo", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html#afc6cf6f0daf588437c8e5284d3dc3ab1", null ],
    [ "DeleteBeszallito", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html#a96384b3119cb1608cc0f59f76ed0703a", null ],
    [ "DeleteCipo", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html#a3fdcac48be9411da4bf184d3c3d6fb56", null ],
    [ "DeleteVasarlo", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html#a0cc55013b4e5a67aa22e4808b174e2ad", null ],
    [ "NegyvenesCipokBeszallitonkent", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html#a50435082696427a7f1f64905ecba4edd", null ],
    [ "ReadAllBeszallito", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html#acbf753bea48de9b54109369b15c0ba03", null ],
    [ "ReadAllCipo", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html#aa71b36fc7e582d210cb92cf1789fcab6", null ],
    [ "ReadAllVasarlo", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html#ac989fcb75acd360ab7b783048988c207", null ],
    [ "ReadBeszallito", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html#acef63beee22a39cbeef8651d88c192b8", null ],
    [ "ReadCipo", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html#ac874c4bbdaf1c991023bf22f26f3a964", null ],
    [ "ReadVasarlo", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html#a3b55794ecd52d6e231c52bd1f55138c1", null ],
    [ "UpdateBeszallitoNepszeruseg", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html#a0ca663845fc683e6eb03d5c4b918a65a", null ],
    [ "UpdateCipoAr", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html#ab8a5f7f16233abfe3e177e607931f4f4", null ],
    [ "UpdateCipoNev", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html#a9534f9221062ddf4fbf5d57d6d94dc8c", null ],
    [ "UpdateVasarloCipo", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html#ae64b22cce9a9515e7bef3dfac775d706", null ]
];