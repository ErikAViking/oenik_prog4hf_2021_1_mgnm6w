var hierarchy =
[
    [ "MyShoeShop.Data.Beszallito", "class_my_shoe_shop_1_1_data_1_1_beszallito.html", null ],
    [ "MyShoeShop.Data.Cipo", "class_my_shoe_shop_1_1_data_1_1_cipo.html", null ],
    [ "DbContext", null, [
      [ "MyShoeShop.Data.ShoeShopDatabaseEntities", "class_my_shoe_shop_1_1_data_1_1_shoe_shop_database_entities.html", null ]
    ] ],
    [ "MyShoeShop.Repository.IRepository< T >", "interface_my_shoe_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "MyShoeShop.Repository.IRepository< Cipo >", "interface_my_shoe_shop_1_1_repository_1_1_i_repository.html", [
      [ "MyShoeShop.Repository.IShoeShopRepository", "interface_my_shoe_shop_1_1_repository_1_1_i_shoe_shop_repository.html", [
        [ "MyShoeShop.Repository.ShouShopRepository", "class_my_shoe_shop_1_1_repository_1_1_shou_shop_repository.html", null ]
      ] ]
    ] ],
    [ "MyShoeShop.Logic.IShoeLogic", "interface_my_shoe_shop_1_1_logic_1_1_i_shoe_logic.html", [
      [ "MyShoeShop.Logic.ShoeLogic", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html", null ]
    ] ],
    [ "MyShoeShop.Logic.Result", "class_my_shoe_shop_1_1_logic_1_1_result.html", null ],
    [ "MyShoeShop.Logic.Tests.Test", "class_my_shoe_shop_1_1_logic_1_1_tests_1_1_test.html", null ],
    [ "MyShoeShop.Data.Vasarlo", "class_my_shoe_shop_1_1_data_1_1_vasarlo.html", null ]
];