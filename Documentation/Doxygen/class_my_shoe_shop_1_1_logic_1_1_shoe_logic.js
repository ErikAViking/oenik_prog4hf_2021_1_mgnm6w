var class_my_shoe_shop_1_1_logic_1_1_shoe_logic =
[
    [ "ShoeLogic", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#ab9449ea18c589c9728db20eb2627567b", null ],
    [ "ShoeLogic", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#ac7259d8fbac5610306e32efafd664d7a", null ],
    [ "CipokAtlagAr", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#a1ca692e33d0b06ee313f50602d368f6d", null ],
    [ "CipokBeszallitonkent", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#aff87f90b89d9ea16755db3aedc4c93c3", null ],
    [ "CreateBeszallito", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#a2b69c5d130353b71af7d6d16486ff0f6", null ],
    [ "CreateCipo", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#a6e51a387801e111065a80d8cf1772b38", null ],
    [ "CreateVasarlo", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#ac602f8d1cf56e3f60eea0558de841f38", null ],
    [ "DeleteBeszallito", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#aa30619ae34055c51c2d31633d2110650", null ],
    [ "DeleteCipo", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#ad1cd0746feb37aac266eb7638ee89563", null ],
    [ "DeleteVasarlo", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#a63de11884bf6c41d300ab1a8305c6630", null ],
    [ "NegyvenesCipokBeszallitonkent", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#acb34c2923ca54b7e61bc02d6acd7b7ae", null ],
    [ "ReadAllBeszallito", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#aee5b8180a9c49922b99d9e618a60e830", null ],
    [ "ReadAllCipo", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#acb57581a5a4ab740714188aae7ea31af", null ],
    [ "ReadAllVasarlo", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#af301f4614ddf070c5b69d551b0982085", null ],
    [ "ReadBeszallito", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#a45c108f54d99be3f25266c12c1a2e330", null ],
    [ "ReadCipo", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#a2660d4f8157870b707085da274e273ae", null ],
    [ "ReadVasarlo", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#a54ff3c7d9be2b8c5c5dc4f7c6fbd5f63", null ],
    [ "UpdateBeszallitoNepszeruseg", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#a710a5639cbccbf912dace70172adb2ca", null ],
    [ "UpdateCipoAr", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#a23a750be55929e5d9923675fd8ebdedb", null ],
    [ "UpdateCipoNev", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#ad9cac8218f426b293b733a697c9e24a4", null ],
    [ "UpdateVasarloCipo", "class_my_shoe_shop_1_1_logic_1_1_shoe_logic.html#af2953718a9588d6e82f939c45c3ea8f0", null ]
];