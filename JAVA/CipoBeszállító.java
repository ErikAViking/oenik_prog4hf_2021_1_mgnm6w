/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author farka
 */
@WebServlet(name = "CipoBeszallito", urlPatterns = {"/CipoBeszallito"})
public class CipoBesz�ll�t� extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/xml;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<cipok>");
            out.println("<cipo>");
            out.println("<cipo_id>"+127+"+<cipo_id>");
            out.println("<nev>"+"Air Max"+"<nev>");
            out.println("<meret>"+40+"<meret>");
            out.println("<fajta>"+"FutóCipő"+"<fajta>");
            out.println("<szin>"+"Kék"+"<szin>");
            out.println("<ar>"+70000+"<ar>");
            out.println("<beszallito_id>"+10+"<beszallito_id>");
            out.println("</cipo>");
            out.println("<cipo>");
            out.println("<cipo_id>"+135+"+<cipo_id>");
            out.println("<nev>"+"Carina"+"<nev>");
            out.println("<meret>"+40+"<meret>");
            out.println("<fajta>"+"Sneaker"+"<fajta>");
            out.println("<szin>"+"Kék"+"<szin>");
            out.println("<ar>"+70000+"<ar>");
            out.println("<beszallito_id>"+20+"<beszallito_id>");
            out.println("</cipo>");
            out.println("<cipo>");
            out.println("<cipo_id>"+149+"+<cipo_id>");
            out.println("<nev>"+"All Star"+"<nev>");
            out.println("<meret>"+40+"<meret>");
            out.println("<fajta>"+"High-Top"+"<fajta>");
            out.println("<szin>"+"Kék"+"<szin>");
            out.println("<ar>"+70000+"<ar>");
            out.println("<beszallito_id>"+40+"<beszallito_id>");
            out.println("</cipo>");
            out.println("<cipo>");
            out.println("<cipo_id>"+159+"+<cipo_id>");
            out.println("<nev>"+"GW500PSB"+"<nev>");
            out.println("<meret>"+40+"<meret>");
            out.println("<fajta>"+"SportCipő"+"<fajta>");
            out.println("<szin>"+"Kék"+"<szin>");
            out.println("<ar>"+70000+"<ar>");
            out.println("<beszallito_id>"+30+"<beszallito_id>");
            out.println("</cipo>");
            out.println("</cipok>");
        }
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
