﻿CREATE TABLE [dbo].[Cipo] (
    [Cipo_ID]       NUMERIC (20) NOT NULL,
    [Nev]           VARCHAR (50) NULL,
    [Meret]         NUMERIC (20) NULL,
    [Fajta]         VARCHAR (50) NULL,
    [Szin]          VARCHAR (50) NULL,
    [Ar]            NUMERIC (20) NULL,
    [Beszallito_ID] NUMERIC (20) NULL,
    CONSTRAINT [cipo_pk] PRIMARY KEY CLUSTERED ([Cipo_ID] ASC),
    CONSTRAINT [beszallito_fk] FOREIGN KEY ([Beszallito_ID]) REFERENCES [dbo].[Beszallito] ([Beszallito_ID])
);

