﻿// <copyright file="Test.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyShoeShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using MyShoeShop.Data;
    using MyShoeShop.Repository;
    using NUnit.Framework;

    /// <summary>
    /// Tests for logic methods.
    /// </summary>
    [TestFixture]
    public class Test
    {
        private Mock<IShoeShopRepository> mockRepo;
        private ShoeLogic logic;

        /// <summary>
        /// Inicializing for the test methods.
        /// </summary>
        [SetUp]
        public void Init()
        {
            this.mockRepo = new Mock<IShoeShopRepository>();
            this.mockRepo.Setup(x => x.ReadAllCipo()).Returns(new List<Cipo>()
            {
                new Cipo() { Cipo_ID = 1, Nev = "Air Max", Meret = 40, Fajta = "Futocipő", Szin = "Red", Ar = 10000, Beszallito_ID = 10 },
                new Cipo() { Cipo_ID = 2, Nev = "Carina", Meret = 41, Fajta = "Sneaker", Szin = "Black", Ar = 20000, Beszallito_ID = 20 },
                new Cipo() { Cipo_ID = 3, Nev = "All Star", Meret = 42, Fajta = "High-Top", Szin = "White", Ar = 30000, Beszallito_ID = 30 },
                new Cipo() { Cipo_ID = 4, Nev = "GW500PSB", Meret = 43, Fajta = "SportCipő", Szin = "Blue", Ar = 40000, Beszallito_ID = 40 },
            });

            this.mockRepo.Setup(x => x.ReadAllBeszallito()).Returns(new List<Beszallito>()
            {
                new Beszallito() { Beszallito_ID = 10, Nev = "Nike" },
            });

            this.logic = new ShoeLogic(this.mockRepo.Object);
        }

        /// <summary>
        /// Test for logic create method.
        /// </summary>
        [Test]
        public void Test_CreateVasarlo()
        {
            this.logic.CreateVasarlo(new Vasarlo() { Nev = "Erik", Szuletesi_datum = DateTime.Now, Lakcim = "itt", Magassag = 186, Nem = "ferfi", Email = "farkaserik24@gmail.com", Cipo_ID = 127 });
            this.mockRepo.Verify(x => x.Create(It.IsAny<Vasarlo>()), Times.Once());
        }

        /// <summary>
        /// Test for logic read method.
        /// </summary>
        [Test]
        public void Test_ReadShoe()
        {
            Cipo cipo = this.logic.ReadCipo(2);

            this.mockRepo.Verify(x => x.ReadAllCipo(), Times.Once);
            Assert.That(cipo.Beszallito_ID == 20);
        }

        /// <summary>
        /// Test for logic read all method.
        /// </summary>
        [Test]
        public void Test_ReadAllShoe()
        {
            List<Cipo> cipok = this.logic.ReadAllCipo();
            this.mockRepo.Verify(x => x.ReadAllCipo(), Times.Once);
            Assert.That(cipok.Count == 4);
        }

        /// <summary>
        /// Test for logic update shoe name method.
        /// </summary>
        [Test]
        public void Test_UpdateNev()
        {
            this.logic.UpdateCipoNev(3, "GW600PSB");
            this.mockRepo.Verify(x => x.UpdateCipoNev(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// Test for logic update shoe price method.
        /// </summary>
        [Test]
        public void Test_UpdateAr()
        {
            this.logic.UpdateCipoAr(3, 50000);
            this.mockRepo.Verify(x => x.UpdateCipoAr(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Test for logic delete method.
        /// </summary>
        [Test]
        public void Test_Delete()
        {
            this.logic.DeleteCipo(4);
            this.mockRepo.Verify(x => x.DeleteCipo(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Test for logic average price method.
        /// </summary>
        [Test]
        public void Test_AtlagAr()
        {
            int atlagAr = this.logic.CipokAtlagAr();
            Assert.That(atlagAr == 25000);
            mockRepo.Verify(x => x.ReadAllCipo(), Times.Once);
        }

        /// <summary>
        /// Test for logic shoes per supplier method.
        /// </summary>
        [Test]
        public void Test_CipokBeszallitonkent()
        {
            var csoportos = this.logic.CipokBeszallitonkent();
            Assert.That(csoportos.Count() == 4);
            mockRepo.Verify(x => x.ReadAllCipo(), Times.Once);
        }

        /// <summary>
        /// Test for logic 40 size shoes per supplier method.
        /// </summary>
        [Test]
        public void Test_NegyvenesCipokBeszallitonkent()
        {
            var negyvenes = this.logic.NegyvenesCipokBeszallitonkent();
            Assert.That(negyvenes.Count() == 1);
            this.mockRepo.Verify(x => x.ReadAllBeszallito(), Times.Once);
            this.mockRepo.Verify(x => x.ReadAllCipo(), Times.Once);
        }
    }
}
