﻿namespace MyShoeShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyShoeShop.Data;

    /// <summary>
    /// IRepository interface.
    /// </summary>
    /// <typeparam name="T"> generic T. </typeparam>
    public interface IRepository<T>
        where T : class
    { }

    /// <summary>
    /// Interface for declaring the mandatory methods for the main repo.
    /// </summary>
    public interface IShoeShopRepository : IRepository<Cipo>
    {
        // CRUD

        /// <summary>
        /// Adding shoe to the db.
        /// </summary>
        /// <param name="shoe"> Adds shoe to the db. </param>
        void Create(Cipo shoe);

        /// <summary>
        /// Adding a new Supplier to the db.
        /// </summary>
        /// <param name="beszallito"> Adds the neww supplier. </param>
        void Create(Beszallito beszallito);

        /// <summary>
        /// Adding a new customer to the db.
        /// </summary>
        /// <param name="vasarlo"> Adds the new customer to the db. </param>
        void Create(Vasarlo vasarlo);

        /// <summary>
        /// returns the shoes from the db to the logic.
        /// </summary>
        /// <returns> The list of shoes </returns>
        List<Cipo> ReadAllCipo();

        /// <summary>
        /// Returns the suppliers from the db to the logic.
        /// </summary>
        /// <returns> The list of supplier. </returns>
        List<Beszallito> ReadAllBeszallito();

        /// <summary>
        /// Returns the customers to the logic.
        /// </summary>
        /// <returns> The list of customers. </returns>
        List<Vasarlo> ReadAllVasarlo();

        /// <summary>
        /// Updates the name of a shoe.
        /// </summary>
        /// <param name="cipoID"> ID of the shoe which is to be updated. </param>
        /// <param name="ujnev"> The shoes new name. </param>
        void UpdateCipoNev(int cipoID, string ujnev);

        /// <summary>
        /// Updates the price of a shoe.
        /// </summary>
        /// <param name="cipoID"> ID of the shoe which is to be updated. </param>
        /// <param name="ujar"> Value of the shoes new price. </param>
        void UpdateCipoAr(int cipoID, int ujar);

        /// <summary>
        /// Updates which shoe the customer wants to buy.
        /// </summary>
        /// <param name="cipo_ID"> Id of the shoe which is to be replaced. </param>
        /// <param name="ujCipoID"> ID of the new shoe. </param>
        void UpdateVasarloCipo(int cipo_ID, int ujCipoID);

        /// <summary>
        /// Updates the popularity of a supplier.
        /// </summary>
        /// <param name="beszallito_ID"> ID of the supplier. </param>
        /// <param name="ujNepszeruseg"> The Value of the supplliers new popularity. </param>
        void UpdateBeszallitoNepszeruseg(int beszallito_ID, string ujNepszeruseg);

        /// <summary>
        /// Deletes a shoe.
        /// </summary>
        /// <param name="cipoID"> ID of the shoe. </param>
        void DeleteCipo(int cipoID);

        /// <summary>
        /// Deletes a supplier.
        /// </summary>
        /// <param name="beszallito_ID"> ID of the supplier. </param>
        void DeleteBeszallito(int beszallito_ID);

        /// <summary>
        /// Deletes a custoer.
        /// </summary>
        /// <param name="email"> Email of the customer. </param>
        void DeleteVasarlo(string email);
    }

    /// <summary>
    /// Main SHoe Shop repository implementing the Crud and other inquirys.
    /// </summary>
    public class ShouShopRepository : IShoeShopRepository
    {
        private ShoeShopDatabaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShouShopRepository"/> class.
        /// </summary>
        /// <param name="db"> ShoeShop database </param>
        public ShouShopRepository(ShoeShopDatabaseEntities db) { this.db = db; }

        /// <inheritdoc/>
        public void Create(Cipo cipo)
        {
            this.db.Cipo.Add(cipo);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Create(Beszallito beszallito)
        {
            this.db.Beszallito.Add(beszallito);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void Create(Vasarlo vasarlo)
        {
            this.db.Vasarlo.Add(vasarlo);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public List<Cipo> ReadAllCipo()
        {
            return this.db.Cipo.ToList();
        }

        /// <inheritdoc/>
        public List<Beszallito> ReadAllBeszallito()
        {
            return this.db.Beszallito.ToList();
        }

        /// <inheritdoc/>
        public List<Vasarlo> ReadAllVasarlo()
        {
            return this.db.Vasarlo.ToList();
        }

        /// <inheritdoc/>
        public void UpdateCipoNev(int cipoID, string ujnev)
        {
            List<Cipo> cipok = this.db.Cipo.ToList();
            cipok.Where(x=>x.Cipo_ID==cipoID).FirstOrDefault().Nev = ujnev;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateCipoAr(int cipoID, int ujar)
        {
            List<Cipo> cipok = this.db.Cipo.ToList();
            cipok.Where(x => x.Cipo_ID == cipoID).FirstOrDefault().Ar = ujar;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateVasarloCipo(int cipoID, int ujCipoID)
        {
            List<Vasarlo> vasarlok = this.db.Vasarlo.ToList();
            vasarlok.Where(x => x.Cipo_ID == cipoID).FirstOrDefault().Cipo_ID = ujCipoID;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateBeszallitoNepszeruseg(int beszallito_ID, string ujNepszeruseg)
        {
            List<Beszallito> beszallitok = this.db.Beszallito.ToList();
            beszallitok.Where(x => x.Beszallito_ID == beszallito_ID).FirstOrDefault().Nepszeruseg = ujNepszeruseg;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void DeleteCipo(int cipoID)
        {
            List<Cipo> cipok = this.db.Cipo.ToList();
            this.db.Cipo.Remove(cipok.Where(x => x.Cipo_ID == cipoID).FirstOrDefault());
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void DeleteBeszallito(int beszallito_ID)
        {
            List<Beszallito> beszallitok = this.db.Beszallito.ToList();
            this.db.Beszallito.Remove(beszallitok.Where(x => x.Beszallito_ID == beszallito_ID).FirstOrDefault());
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void DeleteVasarlo(string email)
        {
            List<Vasarlo> vasarlok = this.db.Vasarlo.ToList();
            this.db.Vasarlo.Remove(vasarlok.Where(x => x.Email.Equals(email)).FirstOrDefault());
            this.db.SaveChanges();
        }
    }
}
