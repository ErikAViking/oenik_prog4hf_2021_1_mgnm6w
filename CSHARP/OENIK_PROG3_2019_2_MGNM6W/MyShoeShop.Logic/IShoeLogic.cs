﻿// <copyright file="IShoeLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyShoeShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyShoeShop.Data;
    using MyShoeShop.Repository;

    /// <summary>
    /// Interface for logic class.
    /// </summary>
    public interface IShoeLogic
    {
        /// <summary>
        /// Calls the create method of the repo.
        /// </summary>
        /// <param name="shoe"> Shoe object which is to be added. </param>
        void CreateCipo(Cipo shoe);

        /// <summary>
        /// Calls the create method of the repo.
        /// </summary>
        /// <param name="beszallito"> Supplier object which is to be added. </param>
        void CreateBeszallito(Beszallito beszallito);

        /// <summary>
        /// Calls the create method of the repo.
        /// </summary>
        /// <param name="vasarlo"> Customer object which is to be added. </param>
        void CreateVasarlo(Vasarlo vasarlo);

        /// <summary>
        /// Calls the read method if the repo.
        /// </summary>
        /// <param name="cipoID"> ID of the desired shoe. </param>
        /// <returns> Returns the desired Shoe object. </returns>
        Cipo ReadCipo(int cipoID);

        /// <summary>
        /// Calls the read method if the repo.
        /// </summary>
        /// <param name="beszallitoID"> ID of the desired supplier. </param>
        /// <returns> Returns the desired supplier. </returns>
        Beszallito ReadBeszallito(int beszallitoID);

        /// <summary>
        /// Calls the read method if the repo.
        /// </summary>
        /// <param name="email"> Email of the desired customer. </param>
        /// <returns> Return the desired customer object. </returns>
        Vasarlo ReadVasarlo(string email);

        /// <summary>
        /// Calls the readall method of the repo.
        /// </summary>
        /// <returns> Return alls the shoes as a list. </returns>
        List<Cipo> ReadAllCipo();

        /// <summary>
        /// Calls the readall method of the repo.
        /// </summary>
        /// <returns> Returns all the suppliers as a list. </returns>
        List<Beszallito> ReadAllBeszallito();

        /// <summary>
        /// Calls the readall method of the repo.
        /// </summary>
        /// <returns> Returns all the customers as a list. </returns>
        List<Vasarlo> ReadAllVasarlo();

        /// <summary>
        /// Calls the update method of the repo.
        /// </summary>
        /// <param name="cipoID"> ID of the to be updated shoe. </param>
        /// <param name="ujnev"> New name of the shoe. </param>
        void UpdateCipoNev(int cipoID, string ujnev);

        /// <summary>
        /// Calls the update method of the repo.
        /// </summary>
        /// <param name="cipoID"> ID of the to be updated shoe. </param>
        /// <param name="ujar"> New price of the shoe. </param>
        void UpdateCipoAr(int cipoID, int ujar);

        /// <summary>
        /// Calls the update method of the repo.
        /// </summary>
        /// <param name="cipoID"> ID of the shoe which is to be replaced. </param>
        /// <param name="ujCipoID"> ID of the new shoe. </param>
        void UpdateVasarloCipo(int cipoID, int ujCipoID);

        /// <summary>
        /// Calls the update method of the repo.
        /// </summary>
        /// <param name="beszallito_ID"> ID of the to be updated supplier. </param>
        /// <param name="ujNepszeruseg"> New popularity description of the supplier. </param>
        void UpdateBeszallitoNepszeruseg(int beszallito_ID, string ujNepszeruseg);

        /// <summary>
        /// Calls the delete method of the repo.
        /// </summary>
        /// <param name="cipoID"> ID if the to be deleted shoe. </param>
        void DeleteCipo(int cipoID);

        /// <summary>
        /// Calls the delete method of the repo.
        /// </summary>
        /// <param name="beszallitoID"> ID of the to be deleted supplier. </param>
        void DeleteBeszallito(int beszallitoID);

        /// <summary>
        /// Calls the delete method of the repo.
        /// </summary>
        /// <param name="email"> Email of the customer. </param>
        void DeleteVasarlo(string email);

        /// <summary>
        /// Returns the average price of the shoes in the db.
        /// </summary>
        /// <returns> Value of the average price. </returns>
        int CipokAtlagAr();

        /// <summary>
        /// Returns the shoes grouped by the suppliers
        /// </summary>
        /// <returns> Suppliers and their shoes. </returns>
        IEnumerable<IGrouping<decimal, Cipo>> CipokBeszallitonkent();

        /// <summary>
        /// Returns how many shoes there are per supplier.
        /// </summary>
        /// <returns> List of result class which contains the name of the supplier and how many 40 size shoes they have. </returns>
        IList<Result> NegyvenesCipokBeszallitonkent();
    }

    /// <summary>
    /// Buisness logic for the shoe shop db.
    /// </summary>
    public class ShoeLogic : IShoeLogic
    {
        /// <summary>
        /// Shoe shop repo.
        /// </summary>
        private IShoeShopRepository shoeShopRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShoeLogic"/> class.
        /// </summary>
        public ShoeLogic()
        {
            this.shoeShopRepo = new ShouShopRepository(new ShoeShopDatabaseEntities());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShoeLogic"/> class with repo.
        /// </summary>
        /// <param name="repo"> Shoe shop repo. </param>
        public ShoeLogic(IShoeShopRepository repo)
        {
            this.shoeShopRepo = repo;
        }

        /// <inheritdoc/>
        public void CreateCipo(Cipo cipo)
        {
            this.shoeShopRepo.Create(cipo);
        }

        /// <inheritdoc/>
        public void CreateBeszallito(Beszallito beszallito)
        {
            this.shoeShopRepo.Create(beszallito);
        }

        /// <inheritdoc/>
        public void CreateVasarlo(Vasarlo vasarlo)
        {
            this.shoeShopRepo.Create(vasarlo);
        }

        /// <inheritdoc/>
        public Cipo ReadCipo(int cipoID)
        {
            var cipok = this.shoeShopRepo.ReadAllCipo();
            return cipok.Where(x => x.Cipo_ID == cipoID).FirstOrDefault();
        }

        /// <inheritdoc/>
        public Beszallito ReadBeszallito(int beszallitoID)
        {
            var beszallitok = this.shoeShopRepo.ReadAllBeszallito();
            return beszallitok.Where(x => x.Beszallito_ID == beszallitoID).FirstOrDefault();
        }

        /// <inheritdoc/>
        public Vasarlo ReadVasarlo(string email)
        {
            var vasarlok = this.shoeShopRepo.ReadAllVasarlo();
            return vasarlok.Where(x => x.Email.Equals(email)).FirstOrDefault();
        }

        /// <inheritdoc/>
        public List<Cipo> ReadAllCipo()
        {
            return this.shoeShopRepo.ReadAllCipo();
        }

        /// <inheritdoc/>
        public List<Beszallito> ReadAllBeszallito()
        {
            return this.shoeShopRepo.ReadAllBeszallito();
        }

        /// <inheritdoc/>
        public List<Vasarlo> ReadAllVasarlo()
        {
            return this.shoeShopRepo.ReadAllVasarlo();
        }

        /// <inheritdoc/>
        public void UpdateCipoNev(int cipoID, string ujNev)
        {
            this.shoeShopRepo.UpdateCipoNev(cipoID, ujNev);
        }

        /// <inheritdoc/>
        public void UpdateCipoAr(int cipoID, int ujAr)
        {
            this.shoeShopRepo.UpdateCipoAr(cipoID, ujAr);
        }

        /// <inheritdoc/>
        public void UpdateVasarloCipo(int cipoID, int ujCipoID)
        {
            this.shoeShopRepo.UpdateVasarloCipo(cipoID, ujCipoID);
        }

        /// <inheritdoc/>
        public void UpdateBeszallitoNepszeruseg(int beszallito_ID, string ujNepszeruseg)
        {
            this.shoeShopRepo.UpdateBeszallitoNepszeruseg(beszallito_ID, ujNepszeruseg);
        }

        /// <inheritdoc/>
        public void DeleteCipo(int cipoID)
        {
            this.shoeShopRepo.DeleteCipo(cipoID);
        }

        /// <inheritdoc/>
        public void DeleteBeszallito(int beszallitoID)
        {
            this.shoeShopRepo.DeleteBeszallito(beszallitoID);
        }

        /// <inheritdoc/>
        public void DeleteVasarlo(string email)
        {
            this.shoeShopRepo.DeleteVasarlo(email);
        }

        /// <inheritdoc/>
        public int CipokAtlagAr()
        {
            List<Cipo> cipok = this.shoeShopRepo.ReadAllCipo();
            int atlagAr = 0;
            foreach (Cipo item in cipok)
            {
                atlagAr += Convert.ToInt32(item.Ar);
            }

            atlagAr = atlagAr / cipok.Count;
            return atlagAr;
        }

        /// <inheritdoc/>
        public IEnumerable<IGrouping<decimal, Cipo>> CipokBeszallitonkent()
        {
            List<Cipo> cipok = this.shoeShopRepo.ReadAllCipo();
            var csoportos = cipok.GroupBy(x => x.Beszallito_ID);
            return csoportos;
        }

        /// <inheritdoc/>
        public IList<Result> NegyvenesCipokBeszallitonkent()
        {
            var cipok = from beszallito in this.shoeShopRepo.ReadAllBeszallito()
                        join cipo in this.shoeShopRepo.ReadAllCipo()
                        on beszallito.Beszallito_ID equals cipo.Beszallito_ID
                        where cipo.Meret == 40
                        group cipo by beszallito.Nev into g
                        select new Result
                        {
                            BeszallitoNev = g.Key,
                            CipoCount = g.Count(),
                        };
            return cipok.ToList();
        }
    }
}
