﻿// <copyright file="IShoeLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyShoeShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyShoeShop.Data;
    using MyShoeShop.Repository;

    /// <summary>
    /// Class for linq method result.
    /// </summary>
    public class Result
    {
        /// <summary>
        /// Name of the supplier.
        /// </summary>
        public string BeszallitoNev { get; set; }

        /// <summary>
        /// Count of the suppliers shoes.
        /// </summary>
        public int CipoCount { get; set; }
    }

}
