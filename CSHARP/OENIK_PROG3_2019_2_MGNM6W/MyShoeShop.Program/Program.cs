﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyShoeShop
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using MyShoeShop.Data;
    using MyShoeShop.Logic;

    /// <summary>
    /// Main program.
    /// </summary>
    internal class Program
    {
        static void Main(string[] args)
        {
            int bemenet;
            do
            {
                bemenet = 11;
                Console.Clear();
                ShoeLogic logic = new ShoeLogic();
                int id = 0;
                Console.WriteLine("Menü:\n0.Kilépés \n1.ReadAll\n2.Read\n3.Create\n4.Update\n6.Delete\n7.Cipő Berendelés(Java végpont)\n8.Cipők átlagára\n9.Beszállítónkénti cipők csoportosítása\n10.40 es méretű cipők beszállítóinak kilistázása");
                try
                {
                    bemenet = int.Parse(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.WriteLine("Ervénytelen bemenet!");
                }

                switch (bemenet)
                {
                    case 1:
                        Console.WriteLine("Melyik tábla érdekel? (cipo, beszallito, vasarlo)");
                        string valasz = Console.ReadLine();
                        if (valasz.Equals("cipo"))
                        {
                            var cipok = logic.ReadAllCipo();
                            foreach (Cipo item in cipok)
                            {
                                Console.WriteLine(item.Cipo_ID + " " + item.Meret + " " + item.Fajta + " " + item.Nev + " " + item.Szin);
                            }
                        }
                        else if (valasz.Equals("beszallito"))
                        {
                            var beszallitok = logic.ReadAllBeszallito();
                            foreach (var item in beszallitok)
                            {
                                Console.WriteLine(item.Nev + " " + item.Szekhely + " " + item.Nepszeruseg + " " + item.Eves_Bevetel);
                            }
                        }
                        else if (valasz.Equals("vasarlo"))
                        {
                            var vasarlok = logic.ReadAllVasarlo();
                            foreach (var item in vasarlok)
                            {
                                Console.WriteLine(item.Nev + " " + item.Szuletesi_datum + " " + item.Magassag + " " + item.Cipo_ID);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Rosszvalasz");
                        }

                        break;
                    case 2:
                        Console.WriteLine("Melyik tábla érdekel? (cipo, beszallito, vasarlo)");
                        valasz = Console.ReadLine();
                        if (valasz.Equals("cipo"))
                        {
                            Console.WriteLine("Mi a cipő ID-ja?");
                            int id1 = int.Parse(Console.ReadLine());
                            var cipo = logic.ReadCipo(id1);
                            Console.WriteLine(cipo.Nev + " " + cipo.Meret + " " + cipo.Szin);
                        }
                        else if (valasz.Equals("beszallito"))
                        {
                            Console.WriteLine("Mi a beszállító ID-ja?");
                            int id1 = int.Parse(Console.ReadLine());
                            var beszallito = logic.ReadBeszallito(id1);
                            Console.WriteLine(beszallito.Nev + " " + beszallito.Nepszeruseg + " " + beszallito.Szekhely);
                        }
                        else if (valasz.Equals("vasarlo"))
                        {
                            Console.WriteLine("Mi a vasarlo emailje?");
                            string id1 = Console.ReadLine();
                            var vasarlo = logic.ReadVasarlo(id1);
                            Console.WriteLine(vasarlo.Nev + " " + vasarlo.Cipo_ID + " " + vasarlo.Nem);
                        }
                        else
                        {
                            Console.WriteLine("Rosszvalasz");
                        }

                        break;
                    case 3:
                        Console.WriteLine("Mit akarsz hozzáadni? (cipo, beszallito, vasarlo)");
                        valasz = Console.ReadLine();
                        if (valasz.Equals("cipo"))
                        {
                            Cipo ujcipo = new Cipo();
                            Console.WriteLine("Kérem adja meg a cipő adatait enterrel elválasztva! (Cipő_ID, Név, Méret, Fajta, Szín, Ár, Beszállító_ID)");
                            ujcipo.Cipo_ID = int.Parse(Console.ReadLine());
                            ujcipo.Nev = Console.ReadLine();
                            ujcipo.Meret = int.Parse(Console.ReadLine());
                            ujcipo.Fajta = Console.ReadLine();
                            ujcipo.Szin = Console.ReadLine();
                            ujcipo.Ar = int.Parse(Console.ReadLine());
                            ujcipo.Beszallito_ID = int.Parse(Console.ReadLine());
                            logic.CreateCipo(ujcipo);
                            Console.WriteLine("Cipő hozzá adva");
                        }
                        else if (valasz.Equals("beszallito"))
                        {
                            Beszallito ujBeszallito = new Beszallito();
                            Console.WriteLine("Kérem adja meg a beszallito adatait enterrel elválasztva! (Beszallito_ID, Név, Székhely, Nepszeruseg, Éves bevétel)");
                            ujBeszallito.Beszallito_ID = int.Parse(Console.ReadLine());
                            ujBeszallito.Nev = Console.ReadLine();
                            ujBeszallito.Szekhely = Console.ReadLine();
                            ujBeszallito.Alapitas_Datuma = DateTime.Now;
                            ujBeszallito.Nepszeruseg = Console.ReadLine();
                            ujBeszallito.Eves_Bevetel = int.Parse(Console.ReadLine());
                            logic.CreateBeszallito(ujBeszallito);
                            Console.WriteLine("Beszallito hozzá adva");
                        }
                        else if (valasz.Equals("vasarlo"))
                        {
                            Vasarlo ujVasarlo = new Vasarlo();
                            Console.WriteLine("Kérem adja meg a vásárló adatait enterrel elválasztva! (email, Név, Születési dátum, Lakcim, magasság, nem, kiválasztott cipo id-ja)");
                            ujVasarlo.Email = Console.ReadLine();
                            ujVasarlo.Nem = Console.ReadLine();
                            ujVasarlo.Szuletesi_datum = Convert.ToDateTime(Console.ReadLine());
                            ujVasarlo.Lakcim = Console.ReadLine();
                            ujVasarlo.Magassag = int.Parse(Console.ReadLine());
                            ujVasarlo.Nem = Console.ReadLine();
                            logic.CreateVasarlo(ujVasarlo);
                            Console.WriteLine("Vasarlo hozzaadva!");
                        }
                        else
                        {
                            Console.WriteLine("Rosszvalasz");
                        }

                        break;
                    case 4:
                        Console.WriteLine("Mit szeretne módosítani? (cipo ar - 1, cipo nev - 2, vasarlo cipoje - 3, beszallito nepszerusege - 4");
                        int valasztott = int.Parse(Console.ReadLine());
                        switch (valasztott)
                        {
                            case 1:
                                Console.Write("Adja meg a cipő ID-ját: ");
                                id = int.Parse(Console.ReadLine());
                                Console.Write("Adja meg az új árat: ");
                                int ujar = int.Parse(Console.ReadLine());
                                logic.UpdateCipoAr(id, ujar);
                                Console.WriteLine("Modosítás végrehajtva!");
                                break;
                            case 2:
                                Console.Write("Adja meg a cipő ID-ját: ");
                                id = int.Parse(Console.ReadLine());
                                Console.Write("Adja meg az új nevet: ");
                                string ujnev = Console.ReadLine();
                                logic.UpdateCipoNev(id, ujnev);
                                Console.WriteLine("Modosítás végrehajtva!");
                                break;
                            case 3:
                                Console.WriteLine("Adja meg a vasarlo cipőjének idját: ");
                                id = int.Parse(Console.ReadLine());
                                Console.WriteLine("Adja meg az uj cipő idját: ");
                                int ujid = int.Parse(Console.ReadLine());
                                logic.UpdateVasarloCipo(id, ujid);
                                Console.WriteLine("Modosítás végrehajtva!");
                                break;
                            case 4:
                                Console.WriteLine("Adja meg a beszállító idját: ");
                                id = int.Parse(Console.ReadLine());
                                Console.WriteLine("Adja meg az uj nepszerűségi leírást: ");
                                string ujleiras = Console.ReadLine();
                                logic.UpdateBeszallitoNepszeruseg(id, ujleiras);
                                Console.WriteLine("Modosítás végrehajtva!");
                                break;
                        }

                        break;
                    case 6:
                        Console.WriteLine("Mit akarsz törölni? (cipo, beszallito, vasarlo)");
                        valasz = Console.ReadLine();
                        if (valasz.Equals("cipo"))
                        {
                            Console.Write("Adja meg a cipő ID-ját: ");
                            id = int.Parse(Console.ReadLine());
                            logic.DeleteCipo(id);
                            Console.WriteLine("Törlés végrehajtva!");
                        }
                        else if (valasz.Equals("beszallito"))
                        {
                            Console.WriteLine("Adja meg a beszallito idjat: ");
                            id = int.Parse(Console.ReadLine());
                            logic.DeleteBeszallito(id);
                            Console.WriteLine("Törlés végrehajtva!");
                        }
                        else if (valasz.Equals("vasarlo"))
                        {
                            Console.WriteLine("Adja meg a vasarlo emailjet: ");
                            string email = Console.ReadLine();
                            logic.DeleteVasarlo(email);
                            Console.WriteLine("Törlés végrehajtva!");
                        }
                        else
                        {
                            Console.WriteLine("Rosszvalasz");
                        }

                        break;
                    case 7:
                        Console.WriteLine("Cipők berendelése...");
                        XDocument xDoc = XDocument.Load("http://localhost:8080/MyShoeShop/CipoBeszallito");
                        Console.WriteLine("Ezek a cipők érkeztek:");
                        var ujcipok = from x in xDoc.Root.Descendants("cipo")
                                      select new Cipo
                                      {
                                          Cipo_ID = int.Parse(x.Element("cipo_id").Value),
                                          Nev = x.Element("nev").Value,
                                          Meret = int.Parse(x.Element("meret").Value),
                                          Fajta = x.Element("fajta").Value,
                                          Szin = x.Element("szin").Value,
                                          Ar = int.Parse(x.Element("ar").Value),
                                          Beszallito_ID = int.Parse(x.Element("beszallito_id").Value),
                                      };
                        foreach (Cipo item in ujcipok)
                        {
                            Console.WriteLine(item.Nev + " " + item.Meret);
                            logic.CreateCipo(item);
                        }

                        break;
                    case 8:
                        Console.WriteLine("Összes cipő átlagára: "+logic.CipokAtlagAr());
                        break;
                    case 9:
                        var csoportos = logic.CipokBeszallitonkent();
                        foreach (var item in csoportos)
                        {
                            if (item.Key == 10)
                            {
                                Console.Write("Nike: ");
                            }
                            else if (item.Key == 20)
                            {
                                Console.Write("Puma: ");
                            }
                            else if (item.Key == 30)
                            {
                                Console.Write("Converse: ");
                            }
                            else
                            {
                                Console.Write("New Ballance: ");
                            }

                            List<Cipo> lista = item.ToList<Cipo>();
                            foreach (Cipo item2 in lista)
                            {
                                Console.Write(item2.Cipo_ID + " ");
                            }

                            Console.WriteLine();
                        }

                        break;
                    case 10:
                        var negyevenescipok = logic.NegyvenesCipokBeszallitonkent();
                        foreach (var item in negyevenescipok)
                        {
                            Console.WriteLine(item.BeszallitoNev + " " + item.CipoCount);
                        }

                        break;
                }

                Console.ReadKey();
            }
            while (bemenet != 0);
        }
    }
}
